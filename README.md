# 2020-025

CDAP Project 

# Smart Plant Disorder Identification

## Group Details

**Supervisor : Dr. Janaka Wijeekon
<br>
Co Supervisor: Dr. Dharshana Kasthurirathna**

**Member Details :** 

| Name | Student ID | Username | Component |
|----|----|----|----|
| M. Sukanya | IT17110808 | sukan1 (Branch : [IT17110808-Component-01](http://gitlab.sliit.lk/group-2020-025/2020-025/tree/IT17110808-Component-01)) | Nutrient deficiency [N,P,K] Identification (Guava, Citrus, Groundnut) |
| L.H. Rajaratne | IT17109840 | Lakna Rajaratne | Degree of disorder identification |
| K.T. Ramasinghe | IT1570418 | Kavindu Tharuka Ramasinghe | IoT device to measure the key nutrients in soil |
| B.I. Sariffodeen | IT17354516 | Bilal Sariffodeen | Blockchain for the interaction between farmer, vendor and experts |


## Project Description

<div align="Justify">
The soil composition around the world is depleting at a rapid rate due to overexploitation by the unsustainable use of fertilizers. Streamlining the availability of nutrient deficiency and fertilizer related knowledge among impoverished farming communities would promoter environmentally and scientifically sustainable farming practices. Thus, contributing to several Sustainable Development Goals set out by the United Nations. The most direct solution to the inappropriate fertilizer usage is to add only the necessary amounts of fertilizer required by plants to produce a significant yield without nutrition deficiencies. To this end this paper proposes a Smart Nutrient Disorder Identification system employing computer vision and machine learning techniques for identification purposes and a decentralized blockchain platform to streamline a bias-less procurement system. The proposed system yielded 88% accuracy in disorder identification, while also enabling secure, transparent flow of verified information. 
</div>


## Main Objective of Research: 

Provide an intelligent solution capable of identifying the nutrition deficiencies in plants, the extent of the nutrition deficiency and recommend the suitable remedy through 
soil analysis in an agriculture researcher verified system.

## Individual Objectives of Research:

1. IT17110808 - M.Sukanya (sukan1)
      - Accurate identification of nutrition deficiency (amongst N, P and K) (The specific objective of this component is to identify the nutrient deficiency in a plant with speed and accuracy using image processing technique and CNN)
<br> <br>

2. IT17109840 - L.H. Rajaratne
      - Identification of the degree of nutrition deficiency and recommendation of suitable remedy (The primary objective of this component is to identify the degree of the disorder. Secondly, this component aims in using the results obtained from component 3, to recommend the suitable remedy in terms of the application of chemical fertilizer etc.)
<br> <br>
      
3. IT15070418 - K.T. Ramasinghe
      - Soil analysis using IoT to provide accurate degree of nutrition deficiency in soil and recommendation (The main expectation of this component is to develop a fully functionally IOT device to measure Nutrient levels (NPK),Organic Matters, EC Conductivity, pH level of selected soil crop and creating inputs for Machine learning algorithm which is created by member 2)
<br> <br>

4. IT17354516 - B.I Sariffodeen
      - Implement a secure and distributed platform for identifying best commercial product for deficiency based on diagnosis (The primary objective of this component is to identify the most suitable methodology to implement a distributed platform to provide farmer – vendor – agricultural researcher engagement while over coming information asymmetry)
<br> <br>


## Main Research Question:

Investigating best technological procedures to accurately detect and verify a plant nutrient deficiency with specific degree and recommend most adequate solutions based on degree of identified deficiency.

### Identified Individual Research Questions:

- Investigating the best machine learning and image processing techniques to be used to select for identification of the nutrition disorder and its degree.

- Investigating the plant nutrition deficiency symptoms and the relevant magnitude of nutrient deficiency in soil.

- Investigating the best method to be used to set up a control experiment to analyse the above variations.

- Investigating the use of Arduino in assessing soil composition.

- Investigating the most practical method to be used to vary soil composition and obtain a usable measure to analyse and use the data on soil composition.

- Investigating the use of block-chain technologies in creating a system for the interaction of stakeholders.

- Investigating the attitudinal manners in promoting this system to be used by the stakeholders. 

### Individual Research Problems:

- The inability to detect a plant with a particular nutrition deficiency specifically amongst a number of deficiencies based on symptoms appearing on the leaf. The inability to detect nutrition deficiencies in this manner amongst a variety of plants subsequently is the first main research problem identified.

- The inability to detect the degree of a plant identified to contain nutrition deficiency accurately and analyse the reason for such a deficiency using several variables. The inability to carry out the above function and recommend a suitable remedy for the deficiency are all part of this same research problem.

- The inability to measure and analyse the soil composition data by means of an IoT device that is practically usable in the field. The unavailability of a solution to provide these values to be used for further analysis on the amount of fertilizer required in a short time with a significant accuracy.

- The inability to provide farmers with verified information by researches on the best fertilizer to be used at a given time without being influenced by the vendors.

## Summery of Individual Components
<br>

- **Identification of Nutrient disorder in plants (amongst N, P and K) - IT17110808 - M.Sukanya (sukan1)**
      <br> <br>
      - [x] <div align="Justify"> For the nutrient deficiency prediction, Images of guava, Citrus and Groundnut were collected and Those images were split among Nitrogen, Potassium and Phosphorus deficiency. That classified images were used for the training and validation Dataset. After collecting the images preprocessing was done to increase the accuracy and reduce the complexity of the dataset. Feature extraction and classification happened within the CNN model. Here two CNN architectures such as EfficientnetB0 and ResNet50 were trained and accuracy has been compared among those. CNN EfficientnetB0 model is proved to be efficient in predicting the nutrient deficiency by comparing with ResNet50 and VGG16. Final outcome of this research is a mobile application named “CropMedic Plus 2.0” which can process the image of the leaf of the plant to detect the nutrient deficiency in it accurately. Current accuracy level of the model is 88% which will be improved in the future work. Mobile app developed using the Flutter framework.
            </div>

> **Model can be downloaded from this** [download link](https://drive.google.com/drive/folders/1o28qFtzjJbYTuFbOe6jKMvfzZ2MSlDnS?usp=sharing)
<br> <br>
> **Datasets used to train the model can be downloaded from this** [download link](https://drive.google.com/drive/folders/1jIgmPJTOkQC1YjMBRsja9uCAS6f5HgN_?usp=sharing)

</div>
<br>


- **Identification of the degree of nutrition deficiency and recommendation of suitable remedy - IT17109840 - L.H. Rajaratne**
      <br> <br>
      - [x] <div align="Justify"> The degree of nutrition disorder means to simply to identify the extent of the color change in a leaf. However, this method is erroneous due to inter and intra-rater variability. Furthermore, such identification is carried out only by specialists which curtails information being available to the general farming population to take timely action. For this purpose, the deep learning specifically used was convolutional networks with the use of a Mask-RCNN with Resnet101 and Feature Pyramid Network (FPN) backbone architecture. This was used in conducting transfer learning on the COCO weights to ensure high accuracy of the results obtained when the actual data set is used. The Restnet101 was chosen due to its high accuracy and its successful implementation against other models 
            </div>
<br>

- **Soil analysis using IoT to provide accurate degree of nutrition deficiency in soil and recommendation - IT15070418 - K.T. Ramasinghe**
      <br> <br>
      - [x] <div align="Justify"> Soil Analysis is basically included with two parts. First one is, IoT device which measures 4 parameters such as soil pH, Electrical Conductivity, Temperature, Moisture. While designing the IoT device the arduino technology has used by the researcher. MAX6675 thermocouple temp sensor, Liquid pH value detection sensor, SEN13322 Moisture sensor and SIM 800L GSM module are the main sensors which have used to develop the device. Making the device with the usage of arduino pro mini leads to make this device cost effective. And second one is, Machine learning algorithm which will predict the NPK percentages of soil by using the values above four parameters. ML model which is running in the hidden layer designed based on machine learning algorithms. Farmer has to enter those parameters to the mobile app. The machine learning algorithm which running on the mobile backend will be predicted NPK level of given soil sample. 
           </div>
<br>

- **Implement a secure and distributed platform for identifying best commercial product for deficiency based on diagnosis - IT17354516 - B.I Sariffodeen**
      <br> <br>
      - [x] <div align="Justify"> xxxxxx
            </div>
<br>


## Technologies Used

![tech](https://user-images.githubusercontent.com/47712418/97636589-58dc9b00-1a5f-11eb-8c4d-8f7994b16fe1.PNG)

## System Architecture

![image](https://user-images.githubusercontent.com/47712418/97927599-4af28700-1d8b-11eb-935d-a04c28219951.png)

## Folder structure

This folder structure adheres to the general project structure of this project. The following list describes the folders and files in use throughout the entire project.

* **Backend - ML Model** 
    - IT17110808 (M.Sukanya - sukan1)
      - CNN - Disorder Identification : Contains the Notebooks of Prediction of disorder
      - CNN - Model Implementation : Contains the notebook of CNN Model training
      <br>
    - IT17109840 (L.H. Rajaratne)
      - Mask RCNN - Degree of disorder Identification : Contains the Python code of Prediction of degree of disorder
      - Mask CNN - Model Implementation : Contains the code of Model training
      <br> <br>

* **Blockchain - Hyperledger Fabric** 
    - IT17354516 (B.I Sariffodeen)
      - contains the implementations of Blockchain component web application.
      <br> <br>

* **crop medic flutter app** 
    - IT17110808 (M.Sukanya)
      - Contains the code for the mobile application for disorder and degree identification.
      <br> <br>

* **Soil analysis - IoT** 
    - IT15070418 (K.T. Ramasinghe)
      - Contains the IoT code for the soil analysis .
      <br> <br>

## Getting Started - crop_medic_flutter_app Mobile Application

<div align="Justify">
The System “Crop Medic Plus 2.0” is a mobile application where user can able to predict the nutrient deficiency and its degree by capturing the image of suspected leaf. Moreover, it was developed using the flutter framework with Dart language. Flutter is an SDK from google to make cross platform application that can run on IOS and Android with a single code base. Firebase is used for the database to store the prediction results. Android studio is used as a development environment. Backend model converted into TFlite and imported as assets in the project. <br> <br> 


**Model used for the prediction in the mobile app can be downloaded from this** [download link](https://drive.google.com/drive/folders/1o28qFtzjJbYTuFbOe6jKMvfzZ2MSlDnS?usp=sharing)
<br> <br>


### Prerequisites

1. Android Studio
2. Flutter 
3. Dart plugins

### Procedure of setting up prerequisites and run the project

- Get the flutter sdk from the get the source code from the Flutter [repo](https://github.com/flutter/flutter)

```
git clone https://github.com/flutter/flutter.git -b stable

```
- Install [Android Studio](https://developer.android.com/studio) version 3.0 or later and Start Android Studio

- Open plugin preferences (Configure > Plugins as of v3.6.3.0 or later)

- Select the Flutter plugin and click Install

- Click Yes when prompted to install the Dart plugin

- Click Restart when prompted

- Download or clone the above project [crop_medic_flutter_app](http://gitlab.sliit.lk/group-2020-025/2020-025.git)

```
git clone http://gitlab.sliit.lk/group-2020-025/2020-025.git

```

- Go to project root and execute the following command in console to get the required dependencies

``` 
flutter packages get 
```

- Run the Project

```
flutter run

```

### Android - Installation

In `android/app/build.gradle`, add the following setting in `android` block.

```
    aaptOptions {
        noCompress 'tflite'
        noCompress 'lite'
    }
```

### App Structure

- `<app_root>/pubspec.yaml`: Used to add packages to the app from pub.dev
- `<app_root>/lib/main.dart`: Used for all the logic of the app
- `<app_root>/lib/models/`: Used to store the model class
- `<app_root>/lib/provider/`: Used to connect the UI and application logic
- `<app_root>/lib/screens/`: It contains the all User interaces
- `<app_root>/lib/screens/auth`: It contains the UI for the Login and Register
- `<app_root>/lib/screens/disorder`: It contains UI for the description of trending disorders
- `<app_root>/lib/validation/`: It contains the field validation used in the project
- `<app_root>/assets/`: used for storing and using TFLite model, label and images used in the app
- `<app_root>/android/app/src/main/AndroidManifest.xml`: used for modifying app name and details
- `<app_root>/android/app/build.gradle`: used for specifying TFLite model not to be compressed


### Dependencies Used

- `cupertino_icons` : ^0.1.2 -  [Download Link](https://pub.dev/packages/cupertino_icons/install)
- `tflite` : ^1.1.1 - [Download Link](https://pub.dev/packages/tflite/install)
- `image_picker` : ^0.6.3 - [Download Link](https://pub.dev/packages/image_picker/install)
- `animated_splash` : ^1.0.0 - [Download Link](https://pub.dev/packages/animated_splash/install)
- `carousel_pro` : ^0.0.13 - [Download Link](https://pub.dev/packages/carousel_pro/install)
- `flutter_speed_dial` : ^1.2.5 - [Download Link](https://pub.dev/packages/flutter_speed_dial/install)
- `flutter_launcher_icons` : ^0.8.1 - [Download Link](https://pub.dev/packages/flutter_launcher_icons/install)
- `firebase_auth` : ^0.14.0+5 - [Download Link](https://pub.dev/packages/firebase_auth/versions/0.14.0+5/install)
- `cloud_firestore` : ^0.12.9+4 - [Download Link](https://pub.dev/packages/cloud_firestore/install)
- `firebase_storage` : ^3.0.6 - [Download Link](https://pub.dev/packages/firebase_storage/install)
- `fluttertoast` : ^4.0.1 - [Download Link](https://pub.dev/packages/fluttertoast/versions/4.0.1/install)

<br>

## Getting Started - Blockchain Hyperledger Fabric Web Application

Hyperledger Fabric (HLF) should be setup in an OS in-order-to construct the blockchain network. In this case, it has been deployed in an Ubuntu environment.The smart contract (chaincode) has been composed using Golang as it is the default programming language used in the chaincode for the first-network 

### Prerequisites

Piror to installing HLF, there are certain prerequisites that should be setup in the Ubuntu environment.

1. curl 
2. docker (chaincode deployed via docker containers)
3. python 2.7.x 
4. nodejs
5. Golang


### Procedure of setting up prerequisites

- Install curl: 
```
sudo apt-get install curl
```
- Setup golang: 
```
sudo apt-get install golang-go
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
```
- Setup nodejs (APIs): 
```
sudo apt-get install nodejs
```

- Node package manager:
```
sudo apt-get install npm
```

- Install python: 

```
sudo apt-get install python (check python version using python --version if 2.7.x version present this step can be skipped)
```

- Setup docker incooperating curl: 

```
sudo apt-get install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable
```

- Download package information from all configured sources: 

```
sudo apt-get update
apt-cache policy docker-ce
sudo apt-get install -y docker-ce
```
 
- Configure docker compose (tool for defining and running multi-container Docker applications): 

```
sudo apt-get install docker-compose
```

- Upgrade existing packages: 

```
sudo apt-get upgrade
```

**Afterwards installation of HLF should be done** 

- HLF Network: (this takes considerable time. Necessary version can be obtained from official HLF documentation)

```
sudo curl -sSL https://goo.gl/6wtTN5 | sudo bash -s 1.1.0 
```

- Contain first-network which is configured for purpose of CropMedic+ 2.0:

```
sudo chmod 777 -R fabric-samples
```

- Once completion is notified, verification can be done by running first network. Go to the first network directory by: 

```
cd fabric-samples/first-network
```

- Run generate script (create the certificates and keys for the entities that are going to exist on 1st blockchain):

```
sudo ./byfn.sh generate
```

- Run first network as:

```
sudo ./byfn.sh up  (Bring it down with ./byfn.sh down)
```
<br>

## Running the blockchain network

- Open the fabric directory followed by first network directory excute following command:
```
./byfn.sh up -s couchdb
```
(this sets up HLF network of 3 peers and there couchdb instances with Certifying Authority and orderer)
use `docker ps` command to view details of network once process completed.

- Run the Following Command to install chaincode on the peer0: (there are 3 peers namely peer0, peer1, peer2)

```
docker exec cli peer chaincode install -n agri -l golang -p github.com/chaincode/agri -v 0.1
```

- peer1:

```
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer1.org1.example.com:8051" cli peer chaincode install -n agri -p github.com/chaincode/agri -v 0.1
```

- peer2: 
```
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer2.org1.example.com/tls/ca.crt" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer2.org1.example.com:9051" cli peer chaincode install -n agri -p github.com/chaincode/agri -v 0.1
```

- To instantiate chaincode on network:

```
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 --cafile $ORDERER_CA -C mychannel -c '{"Args":[]}' -n agri -v 0.1 -P "OR('Org1MSP.member')
``` 
```
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```

Once this is complete, the custom blockchain network would be running.


<br>

## To get more information about this research

> ### https://sukan.github.io/CropMedicPlus2.0_web/
